"""Cmdlr infomation files."""

VERSION = (4, 1, 4)
DESCRIPTION = ('Extensible comic subscriber.')
LICENSE = 'MIT'
AUTHOR = 'visig'
AUTHOR_EMAIL = 'visig@protonmail.ch'
PROJECT_URL = 'https://gitlab.com/visig/cmdlr'
PROJECT_NAME = 'cmdlr'
